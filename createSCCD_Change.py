import time
import os
import os.path
import logging
from threading import Thread
import sys
from time import sleep
from datetime import datetime
import urllib2
from base64 import b64encode


maximo_conn='https://'
maximo_ip='81.95.150.45'
maximo_port='9443'
maximo_restctx='/maxrest'
maximo_rest_uri=maximo_conn+maximo_ip+':'+maximo_port+maximo_restctx
maximo_srv_url=maximo_conn+maximo_ip+':'+maximo_port

def createSCCDChange(changeID,description):

    try:
        dateTime = time.strftime("%Y-%m-%dT%H:%M:%S")
        logging.debug(dateTime)
        user = 'maxadmin'
        password = 'maximo@123'
        reportedUser = 'maxadmin'
        priority = '1'
        siteID = 'SITE1'
        impact = '2'
        xmldata = '<max:CreateMXOSCHANGE xmlns:max="http://www.ibm.com/maximo">\
            <max:MXOSCHANGESet>\
              <WOCHANGE>\
              <WONUM>' + changeID + '</WONUM>\
              <SITEID>' + siteID + '</SITEID>\
              <DESCRIPTION>' + description + '</DESCRIPTION>\
              <STATUS>WAPPR</STATUS>\
              <STATUSDATE>'+ dateTime + '</STATUSDATE>\
              <PMCOMIMPACT>' + impact + '</PMCOMIMPACT>\
            </WOCHANGE>\
          </max:MXOSCHANGESet>\
        </max:CreateMXOSCHANGE>'

        logging.debug("server url -> " + maximo_srv_url)
        request = urllib2.Request(maximo_srv_url+'/meaweb/os/mxoschange',data=xmldata)
        request.get_method = lambda: 'POST'
        request.add_header('Authorization', 'Basic ' + b64encode(user + ':' + password))
        r = urllib2.urlopen(request)
        return changeID
    except Exception,e:
        #transaction.rollback()
        logging.debug("Exception while creating Change")
        logging.debug(e)


def createSCCDIncident(ticketID,createdby):

    try:   
        dateTime = time.strftime("%Y-%m-%dT%H:%M:%S")
        user = 'maxadmin'
        password = 'maximo@123'
        reportedUser = 'Binesh'
        priority = '1'
        xmldata = '<max:CreateMXOSTICKET xmlns:max="http://www.ibm.com/maximo">\
          <max:MXOSTICKETSet>\
          <TICKET>\
            <CLASS>INCIDENT</CLASS>\
            <STATUS>NEW</STATUS>\
            <STATUSDATE>' + str(dateTime) + '</STATUSDATE>\
            <ISGLOBAL>0</ISGLOBAL>\
            <RELATEDTOGLOBAL>0</RELATEDTOGLOBAL>\
            <SITEVISIT>0</SITEVISIT>\
            <INHERITSTATUS>1</INHERITSTATUS>\
            <ISKNOWNERROR>0</ISKNOWNERROR>\
            <CHANGEDATE>' + str(dateTime) + '</CHANGEDATE>\
            <CHANGEBY>MAXADMIN</CHANGEBY>\
            <HISTORYFLAG>0</HISTORYFLAG>\
            <TEMPLATE>0</TEMPLATE>\
            <HASACTIVITY>0</HASACTIVITY>\
            <ACTLABHRS>0.0</ACTLABHRS>\
            <ACTLABCOST>0.00</ACTLABCOST>\
            <LANGCODE>EN</LANGCODE>\
            <HASLD>1</HASLD>\
            <SELFSERVSOLACCESS>0</SELFSERVSOLACCESS>\
            <HASSOLUTION>0</HASSOLUTION>\
            <PLUSPADDRISCHANGED>0</PLUSPADDRISCHANGED>\
            <PLUSPPOREQ>0</PLUSPPOREQ>\
            <ESCALATED>0</ESCALATED>\
            <INPROGEMAIL>0</INPROGEMAIL>\
            <INCIDENTBYCLASS>0</INCIDENTBYCLASS>\
            <PLUSCUSTOMER>SL1</PLUSCUSTOMER>\
            <REPORTEDBY>BJIN.IBM.COM</REPORTEDBY>\
            <INTERNALPRIORITY>1</INTERNALPRIORITY>\
          </TICKET>\
          </max:MXOSTICKETSet>\
        </max:CreateMXOSTICKET>'

        logging.debug("XMLDATA for ticket " + xmldata)
       
        request = urllib2.Request(maximo_srv_url+'/meaweb/os/mxosticket',data=xmldata)
        request.get_method = lambda: 'POST'
        request.add_header('Authorization', 'Basic ' + b64encode(user + ':' + password))
        r = urllib2.urlopen(request)
    #    return HttpResponse(r)
    except:
        #transaction.rollback()
        logging.debug("Exception while creating Incident")


##Method to add Incident/Change in DB
#Method to retrieve last ticket in Maximo
import json
def fetchSCCDLastIncident():
    loggingin_cred = { 'user' : 'maxadmin' , 'password' : 'maximo@123' }
    user = loggingin_cred['user']
    password = loggingin_cred['password']
    reportedUser = 'Mani'
    json_params = '_format=json&_includecols=TICKETUID&_maxItems=1&_rsStart=0&_orderbydesc=TICKETUID'
    req_url = maximo_rest_uri+'/rest/mbo/ticket'
    logging.debug("Request URI -> " + json_params )
    #try: 
    if 1==1:
        request = urllib2.Request(req_url + '?' + json_params)
        request.get_method = lambda: 'GET'
        request.add_header('Authorization', 'Basic ' + b64encode(user + ':' + password))
        r = urllib2.urlopen(request)
    req_data = json.load(r)
    return req_data['TICKETMboSet']['TICKET'][0]['Attributes']['TICKETUID']['content'] 


def fetchSCCDLastChange():
    loggingin_cred = { 'user' : 'maxadmin' , 'password' : 'maximo@123' }
    user = loggingin_cred['user']
    password = loggingin_cred['password']
    reportedUser = 'Mani'
    json_params = '_format=json&_includecols=WONUM&_maxItems=1&_rsStart=0&_orderbydesc=WONUM'
    req_url = maximo_rest_uri+'/rest/mbo/workorder'
    logging.debug("Request URI -> " + json_params )
    try:
        request = urllib2.Request(req_url + '?' + json_params)
        request.get_method = lambda: 'GET'
        request.add_header('Authorization', 'Basic ' + b64encode(user + ':' + password))
        r = urllib2.urlopen(request)
        req_data = json.load(r)
        return req_data['WORKORDERMboSet']['WORKORDER'][0]['Attributes']['WONUM']['content']
    except Exception,e:
        logging.debug("Exception while fetching last change data ")
        logging.debug(e)


#@transaction.commit_manually
def insertTicket(operation):
  #saved=False
  ticketingTool = "SCCD"
  #ticketingTool = "SoftLayer"
  if (ticketingTool == "SCCD"):
    try:
        #id = "11113"
        #changeID = "22223"
        #nextid = str(int(fetchSCCDLastIncident()) + 1)
        # Because change entries are not being made using incident id to increment
        #nextchangeID = str(int( nextid ) + 1)
        #logging.debug(" Next Incident to create " + nextid )
        nextchangeID = str(int(fetchSCCDLastChange()) + 1)
        logging.debug(" Next Change to create " + nextchangeID + " For Trigger " + operation)
        #createSCCDIncident('111',createdby)
        change=createSCCDChange(nextchangeID, operation)
        return change
    except Exception,e:
        #transaction.rollback()
        logging.debug("Exception while creating Ticket")
        logging.debug(e)

logging.basicConfig(filename='sccd.log',level=logging.DEBUG)
tname=''
arglist=sys.argv[1:-1]
deviceName=sys.argv[-1]
for a in arglist:
    tname=tname+' '+str(a)

if deviceName not in tname[1:]:
    trigger=tname[1:] + ' on ' + deviceName
else:
    trigger=tname[1:]

logging.debug("Creating SCCD Change for Trigger " + trigger)
change=insertTicket(trigger)
logging.debug(change)